package service;

import support.enums.ApiPath;
import support.enums.BaseUri;

import java.util.HashMap;
import java.util.Map;

import static support.utils.RestContext.*;

public class FilmeService {

    public static void buscarFilmePorId(String id) {
        initRequest();
        setPath(BaseUri.URL_BASE.getPath(), ApiPath.GET_FILME.getPath());
        Map<String, String> pathParams = new HashMap<String, String>();
        pathParams.put("codigo", id);
        setPathParams(pathParams);
        getRequest();
    }

    public static void buscarTodosOsFilmes() {
        initRequest();
        setPath(BaseUri.URL_BASE.getPath(), ApiPath.GET_FILMES.getPath());
        getRequest();
    }

    public static void criarUmFilme(Integer id, String nome, String sinopse, String faixaEtaria, String genero) {
        initRequest();
        setPath(BaseUri.URL_BASE.getPath(), ApiPath.POST_CRIAR_FILMES.getPath());
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-type", "application/json");
        setHeader(header);
        setBody("{\n" +
                "    \"codigo\": " + id + ",\n" +
                "    \"nome\": \"" + nome + "\",\n" +
                "    \"sinopse\": \"" + sinopse + "\",\n" +
                "    \"faixaEtaria\": \""+ faixaEtaria +"\",\n" +
                "    \"genero\": \""+ genero +"\"\n" +
                "}");
        postRequest();
    }

    public static void deletarUmFilme(String id) {
        initRequest();
        setPath(BaseUri.URL_BASE.getPath(), ApiPath.DELETAR_FILME.getPath());
        Map<String, String> pathParams = new HashMap<String, String>();
        pathParams.put("codigo", id);
        setPathParams(pathParams);

        deleteRequest();
    }

    public static void editarUmFilme(Integer id, String nome, String sinopse, String faixaEtaria, String genero) {
        initRequest();
        setPath(BaseUri.URL_BASE.getPath(), ApiPath.DELETAR_FILME.getPath());
        Map<String, String> pathParams = new HashMap<String, String>();
        pathParams.put("codigo", "\""+ id + "\"");
        setPathParams(pathParams);
        setBody("{\n" +
                "    \"codigo\": " + id + ",\n" +
                "    \"nome\": \"" + nome + "\",\n" +
                "    \"sinopse\": \"" + sinopse + "\",\n" +
                "    \"faixaEtaria\": \""+ faixaEtaria +"\",\n" +
                "    \"genero\": \""+ genero +"\"\n" +
                "}");
        deleteRequest();
    }
}
