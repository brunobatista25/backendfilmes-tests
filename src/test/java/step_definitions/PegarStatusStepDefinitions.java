package step_definitions;

import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

public class PegarStatusStepDefinitions {
    ValidatableResponse response;

    @Quando("faço uma requisicao para a url de status")
    public void faço_uma_requisicao_para_a_url_de_status() {
    //    RestAssured.given().when().get("http://localhost:8080/status/").then().statusCode(200);

        response =  RestAssured.given()
                .when()
                .get("http://localhost:8080/status/")
                .then().log().all();
    }

    @Então("valido se a resposta foi com status correto")
    public void valido_se_a_resposta_foi_com_status_correto() {
        assertEquals(200,response.extract().statusCode());
        assertEquals("A aplicação está de pé",response.extract().body().asPrettyString());
    }

}
