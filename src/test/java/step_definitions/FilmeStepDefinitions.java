package step_definitions;

import com.github.javafaker.Faker;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.response.ValidatableResponse;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static service.FilmeService.*;
import static support.utils.GetProperties.getProp;
import static support.utils.RestContext.getResponse;

public class FilmeStepDefinitions {

    ValidatableResponse response;
    static Faker faker = new Faker();
    static Integer numero = faker.random().nextInt(1,100);

    @Quando("faço uma requisicao para pegar um filme")
    public void faço_uma_requisicao_para_pegar_um_filme() {
        System.out.println(getProp("urlBase"));
        buscarFilmePorId("1");
        response = getResponse().then().log().all();

    }
    @Então("valido se a resposta veio com o filme correto")
    public void valido_se_a_resposta_veio_com_o_filme_correto() {
        assertEquals(200,response.extract().statusCode());
        assertEquals(Integer.valueOf(1), response.extract().path("codigo"));
        assertEquals("TesteNome", response.extract().path("nome"));
        assertEquals("TesteSinopse", response.extract().path("sinopse"));
        assertEquals("14", response.extract().path("faixaEtaria"));
        assertEquals("TesteGenero", response.extract().path("genero"));
    }

    @Quando("faço uma requisicao para pegar todos os filme")
    public void faço_uma_requisicao_para_pegar_todos_os_filme() {
        buscarTodosOsFilmes();
        response = getResponse().then().log().all();
    }

    @Então("valido se a resposta veio com todos os filmes")
    public void valido_se_a_resposta_veio_com_todos_os_filmes() throws IOException, JSONException {
        assertEquals(200,response.extract().statusCode());

        assertEquals(Integer.valueOf(1), response.extract().path("[0].codigo"));
        assertEquals("TesteNome", response.extract().path("[0].nome"));
        assertEquals("TesteSinopse", response.extract().path("[0].sinopse"));
        assertEquals("14", response.extract().path("[0].faixaEtaria"));
        assertEquals("TesteGenero", response.extract().path("[0].genero"));

        assertEquals(Integer.valueOf(2), response.extract().path("[1].codigo"));
        assertEquals("TesteNome2", response.extract().path("[1].nome"));
        assertEquals("TesteSinopse2", response.extract().path("[1].sinopse"));
        assertEquals("12", response.extract().path("[1].faixaEtaria"));
        assertEquals("TesteGenero2", response.extract().path("[1].genero"));

        assertEquals(Integer.valueOf(3), response.extract().path("[2].codigo"));
        assertEquals("TesteNome3", response.extract().path("[2].nome"));
        assertEquals("TesteSinopse3", response.extract().path("[2].sinopse"));
        assertEquals("13", response.extract().path("[2].faixaEtaria"));
        assertEquals("TesteGenero3", response.extract().path("[2].genero"));

        // ou poderia pegar por json
       // String loading = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/filmes.json")));
       // JSONAssert.assertEquals(loading, getResponse().then().extract().asString(), true);
    }


    @Quando("faço uma requisicao para criar um filme")
    public void faço_uma_requisicao_para_criar_um_filme() {
        System.out.println(numero);
        criarUmFilme(numero, "filme do bruno","assassinando os devs", "18", "terror");
        response = getResponse().then().log().all();
    }

    @Então("valido se o filme foi criado com sucesso")
    public void valido_se_o_filme_foi_criado_com_sucesso() {
        assertEquals(201,response.extract().statusCode());
        assertEquals(Integer.valueOf(numero), response.extract().path("codigo"));
        assertEquals("filme do bruno", response.extract().path("nome"));
        assertEquals("assassinando os devs", response.extract().path("sinopse"));
        assertEquals("18", response.extract().path("faixaEtaria"));
        assertEquals("terror", response.extract().path("genero"));
    }

    @Quando("faço uma requisicao para criar um filme faltando campos {string} {string} {string} {string}")
    public void faço_uma_requisicao_para_criar_um_filme_faltando_campos(String nome, String  sinopse, String faixaEtaria, String genero) {
        criarUmFilme(106, nome, sinopse, faixaEtaria, genero);
        response = getResponse().then().log().all();
    }

    @Então("valido a {string} de erro")
    public void valido_a_de_erro(String message) {
        assertEquals(message, response.extract().jsonPath().get("message"));
    }

    @Dado("que eu tenho um filme criado")
    public void que_eu_tenho_um_filme_criado() {
        criarUmFilme(numero, "filme do bruno","assassinando os devs", "18", "terror");
        response = getResponse().then().log().all();
    }

    @Quando("faço uma requisicao para deletar um filme")
    public void faço_uma_requisicao_para_deletar_um_filme() {
        Object teste = response.extract().jsonPath().get("codigo");
        String teste2 = String.valueOf(teste);
        deletarUmFilme(teste2);


    }

    @Então("valido se o filme foi deletado com sucesso")
    public void valido_se_o_filme_foi_deletado_com_sucesso() {
        response = getResponse().then().statusCode(200);
    }

    @Quando("faço uma requisicao para editar um filme com o put")
    public void faço_uma_requisicao_para_editar_um_filme_com_o_put() {

    }

    @Então("valido se o filme foi editado com o put com sucesso")
    public void valido_se_o_filme_foi_editado_com_o_put_com_sucesso() {

    }

    @Quando("faço uma requisicao para editar um filme com o patch")
    public void faço_uma_requisicao_para_editar_um_filme_com_o_patch() {

    }

    @Então("valido se o filme foi editado com o patch com sucesso")
    public void valido_se_o_filme_foi_editado_com_o_patch_com_sucesso() {

    }
}
