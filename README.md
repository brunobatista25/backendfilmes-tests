1 Comecar criando um projeto maven File > new project > maven > nome do projeto

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>automacaobrunao</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.plugin.version>2.22.2</maven.plugin.version>
        <maven.clean.version>3.1.0</maven.clean.version>
        <maven.compiler.version>3.8.0</maven.compiler.version>
        <maven.source.version>1.8</maven.source.version>
        <maven.target.version>1.8</maven.target.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <junit.version>4.13</junit.version>
        <lombok.version>1.18.10</lombok.version>
        <hamcrest.version>2.2</hamcrest.version>
        <restassured.version>4.4.0</restassured.version>
        <javafaker.version>1.0.2</javafaker.version>
        <jsonassert.version>1.5.0</jsonassert.version>
        <cucumber.version>7.6.0</cucumber.version>
    </properties>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/junit/junit -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/io.rest-assured/rest-assured -->
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>${restassured.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.github.javafaker/javafaker -->
        <dependency>
            <groupId>com.github.javafaker</groupId>
            <artifactId>javafaker</artifactId>
            <version>${javafaker.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.skyscreamer/jsonassert -->
        <dependency>
            <groupId>org.skyscreamer</groupId>
            <artifactId>jsonassert</artifactId>
            <version>${jsonassert.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-junit -->
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>${cucumber.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-java -->
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>${cucumber.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.hamcrest/hamcrest -->
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>



    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.version}</version>
                <configuration>
                    <encoding>${project.build.sourceEncoding}</encoding>
                    <source>${maven.source.version}</source>
                    <target>${maven.target.version}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.plugin.version}</version>
                <configuration>
                    <includes>
                        <include>**/RunnerTest.java</include>
                    </includes>
                </configuration>
            </plugin>
        </plugins>

    </build>

</project>
```

ele vai pedir pra vc instalar dois plugins `cucumber for java` e `gherkin`

agora vamos criar nosso primeiro cenario dentro de test > resources > features
E criar o arquivo getStatus.feature

```
#language: pt
  Funcionalidade: Pegar Status do servidor

    Cenario: Validar status do servidor
    Quando faço uma requisicao para a url de status
      Então valido se a resposta foi com status "200"
```

Agora vamos criar o arquivo RunnerTest  dentro da pasta runner para rodar a suite de testes do projeto

```
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features"}
)
public class RunnerTest {
}


```
vamos rodar que ele vai gerar os steps
Agora vamos criar os step_definitions e criar o arquivo PegarStatusStepDefinitions e colar os steps

```
@Quando("faço uma requisicao para a url de status")
public void faço_uma_requisicao_para_a_url_de_status() {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}
@Então("valido se a resposta foi com status {string}")
public void valido_se_a_resposta_foi_com_status(String string) {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}

```

depois disso vamos adicionar o glue para dizer onde esta os steps 

```
,
        glue = {"step_definitions"}
```

Agora vamos criar os testes

```
package step_definitions;

import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

public class PegarStatusStepDefinitions {
    ValidatableResponse response;

    @Quando("faço uma requisicao para a url de status")
    public void faço_uma_requisicao_para_a_url_de_status() {
    //    RestAssured.given().when().get("http://localhost:8080/status/").then().statusCode(200);

        response =  RestAssured.given()
                .when()
                .get("http://localhost:8080/status/")
                .then().log().all();
    }
    
    @Então("valido se a resposta foi com status correto")
    public void valido_se_a_resposta_foi_com_status_correto() {
        assertEquals(200,response.extract().statusCode());
        assertEquals("A aplicação está de pé",response.extract().body().asPrettyString());
    }

}

```

Agora vamos criar o arquivo PegarAmbiente  dentro da pasta utils para pegar o ambiente executado pelo usuario

Vamos criar o pacote support > utils > Environments

```
package support.utils;

public class Environments {
    public static String getEnvironments() {
        String environments = System.getProperty("ambiente");
        return environments;
    }
}
```

agora que pegamos o ambiente temos que passar esse ambiente para dizer qual arquivo properties serao usados

entao vamos criar os arquivos properties local e homol dentro de resources

e adicionar o valor `baseUrl=http://localhost:8080/`

vamos criar o arquivo getProperties para pegar o valor que foi passado pelo ambiente e escolher o arquivo

GetProperties

```
package support.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static support.utils.Environments.getEnvironments;

public class GetProperties {


    private static final String DATA_CONFIG = "src/test/resources/";
    private static Properties properties;

    public static String getProp(String value) {
        try {
            if (properties == null) {
                GetProperties.properties = new Properties();
                GetProperties.properties.load(new FileInputStream(DATA_CONFIG + getEnvironments() +".properties"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return GetProperties.properties.getProperty(value);
    }
}

```

manda printar so pra verem como funciona

```
System.out.println(getProp("baseUrl"));
```

feito isso agora vamos criar os enums de url
criar uma pacote support > enums > BaseUri

```
package support.enums;

import static support.utils.GetProperties.getProp;

public enum BaseUri {
    URL_BASE(getProp("urlBase"));

    private String path;

    BaseUri(String path){
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }
}
```

e vamos crriar o path tbm

```
package support.enums;

public enum ApiPath {

    GET_FILME("/filme/{codigo}");

    private String path;

    ApiPath(String path){
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }
}

```

agora vamos fazer a parte do rest assured e deixar melhor usavel
vamso criar um arquivo utils > RestContext

```
package support.utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

public class RestContext {

    private static RequestSpecification request;
    private static Response response;

    public static void initRequest(){
        RestAssured.useRelaxedHTTPSValidation();
        request = RestAssured.given();
    }

    public static void log(){
        RestAssured.given().then().log().all();
    }

    public static void setPath(String baseUri, String path){
        if ( request == null){
            initRequest();
        }
        request.baseUri(baseUri);
        request.basePath(path);
        RestAssured.useRelaxedHTTPSValidation();
    }

    public static void setHeader(Map<String, String> contentHeader){
        request.headers(contentHeader);
    }
    
    public static void setBody(String contentBody){
        request.body(contentBody);
    }

    public static void getRequest(){
        response = request.get();
    }

    public static void postRequest(){
        response = request.post();
    }

    public static void setPathParams(Map<String, String> params){
        request.pathParams(params);
    }

    public static void setParams(Map<String, ?> params){
        request.params(params);
    }

    public static void putRequest(){
        response = request.put();
    }

    public static void patchRequest(){
        response = request.patch();
    }

    public static Response getResponse(){
        return response;
    }
}

```

agora vamos criar o service para juntar os comandos e fazer as requisicoes

service > FilmeService

```
```

mostrar como pega por json

String loading = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/usuario.json")));

//falso nao verifica todos os campos
JSONAssert.assertEquals(sucesso, getResponse().then().extract().asString(), false);

//true  verifica todos os campos
JSONAssert.assertEquals(sucesso, getResponse().then().extract().asString(), true);