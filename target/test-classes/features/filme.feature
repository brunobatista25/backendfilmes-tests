#language: pt
Funcionalidade: Pegar Status do servidor

#  Cenario: Validar buscar um filme com sucesso
#    Quando faço uma requisicao para pegar um filme
#    Então valido se a resposta veio com o filme correto
#
#  Cenario: Validar buscar todos os filme com sucesso
#    Quando faço uma requisicao para pegar todos os filme
#    Então valido se a resposta veio com todos os filmes
#
#  Cenario: Criar um filme com sucesso
#    Quando faço uma requisicao para criar um filme
#    Então valido se o filme foi criado com sucesso
#
#  Esquema do Cenario: Tentar criar um filme faltando campos
#    Quando faço uma requisicao para criar um filme faltando campos "<nome>" "<sinopse>" "<faixaEtaria>" "<genero>"
#    Então valido a "<mesangem>" de erro
#
#    Exemplos:
#    |nome     |sinopse     |faixaEtaria     |genero     |mesangem                   |
#    |         |sinopseTeste|faixaetariaTeste|generoTeste|Nome é obrigatório!        |
#    |nometeste|            |faixaetariaTeste|generoTeste|Sinopse é obrigatório!     |
#    |nometeste|sinopseTEste|                |generoTeste|Faixa etária é obrigatória!|
#    |nometeste|sinopseTEste|faixaetariaTeste|           |Genêro é obrigatório!      |

  Cenario: Deletar um filme com sucesso
    Dado que eu tenho um filme criado
    Quando faço uma requisicao para deletar um filme
    Então valido se o filme foi deletado com sucesso

#  Cenario: Editar um filme com put com sucesso
#    Dado que eu tenho um filme criado
#    Quando faço uma requisicao para editar um filme com o put
#    Então valido se o filme foi editado com o put com sucesso
#
#  Cenario: Editar um filme com patch com sucesso
#    Dado que eu tenho um filme criado
#    Quando faço uma requisicao para editar um filme com o patch
#    Então valido se o filme foi editado com o patch com sucesso